package com.springbootrest.app.controller;

import static org.mockito.Mockito.when;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springbootrest.app.controller.AddressController;
import com.springbootrest.app.entity.Address;
import com.springbootrest.app.service.AddressService;

@RunWith(SpringRunner.class)
@WebMvcTest(AddressController.class)
public class AddressControllerTest {

    @Autowired
    private MockMvc mvc;
    
    @MockBean
    private AddressService addressService;

    @Test
    public void createAnValidAddressShouldSuccess()
      throws Exception {
    	Address returnAddress = new Address();
    	returnAddress.setCity("CityName");
    	returnAddress.setState("SC");
    	returnAddress.setId(1L);
    	
    	when(addressService.createAddress(any())).thenReturn(returnAddress);
    	
    	String json = "{\"city\":\"CityName\",\"state\":\"SC\"}";
    	this.mvc.perform(post("/v1/addresses").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
	    	.andExpect(jsonPath("$.city").value("CityName"))
            .andExpect(jsonPath("$.state").value("SC"));
    }
    
    @Test
    public void createANotValidAddressCityShouldReturnBadRequest()
      throws Exception {
    	Address givenAddress = new Address();
    	givenAddress.setCity("City that has more than 30 characters");
    	givenAddress.setState("SC");
    	
    	String json = (new ObjectMapper()).writeValueAsString(givenAddress);
    	this.mvc.perform(post("/v1/addresses").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isBadRequest());
    }
    
    @Test
    public void createANotValidAddressStateShouldReturnBadRequest()
      throws Exception {
    	Address givenAddress = new Address();
    	givenAddress.setCity("City");
    	givenAddress.setState("SCC");
    	
    	String json = (new ObjectMapper()).writeValueAsString(givenAddress);
    	this.mvc.perform(post("/v1/addresses").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isBadRequest());
    }
}