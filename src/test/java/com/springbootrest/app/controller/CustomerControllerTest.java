package com.springbootrest.app.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.springbootrest.app.controller.CustomerController;
import com.springbootrest.app.entity.Address;
import com.springbootrest.app.entity.Customer;
import com.springbootrest.app.service.CustomerService;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @Autowired
    private MockMvc mvc;
    
    @MockBean
	private CustomerService customerService;
    
    @Test
    public void createAValidCustomerShouldSuccess()
      throws Exception {
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    	Date date = format.parse("1991-04-14");
    	
    	Customer returnCustomer = new Customer();
    	returnCustomer.setName("Customer Name");
    	returnCustomer.setBirthDate(date);
    	returnCustomer.setGender("F");
    	Address address = new Address(56L, "City Name", "SC");
    	returnCustomer.setAddress(address);
    	
    	when(customerService.createCustomer(any())).thenReturn(returnCustomer);
    	
    	String json = "{ \"addressId\": 56, \"birthDate\": \"1991-04-14\", \"gender\": \"F\", \"name\": \"Customer Name\" }";
    	this.mvc.perform(post("/v1/customers").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
	    	.andExpect(jsonPath("$.name").value("Customer Name"))
            .andExpect(jsonPath("$.gender").value("F"))
            .andExpect(jsonPath("$.birthDate").value("1991-04-14T00:00:00.000+0000"))
            .andExpect(jsonPath("$.address.id").value("56"))
            .andExpect(jsonPath("$.address.city").value("City Name"))
            .andExpect(jsonPath("$.address.state").value("SC"));
    }
    
    @Test
    public void createAnInvalidCustomerBirthdayShouldBadRequest()
      throws Exception {
    	String json = "{ \"addressId\": 56, \"birthDate\": \"19919-04-14\", \"gender\": \"F\", \"name\": \"Customer Name\" }";
    	this.mvc.perform(post("/v1/customers").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isBadRequest());
    }
    
    @Test
    public void createAnInvalidCustomerGenderShouldBadRequest()
      throws Exception {
    	String json = "{ \"addressId\": 56, \"birthDate\": \"1991-04-14\", \"gender\": \"FM\", \"name\": \"Customer Name\" }";
    	this.mvc.perform(post("/v1/customers").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isBadRequest());
    }
    
    @Test
    public void createAnInvalidNAmeShouldBadRequest()
      throws Exception {
    	String json = "{ \"addressId\": 56, \"birthDate\": \"1991-04-14\", \"gender\": \"F\", \"name\": "
    			+ "\"Customer Name that exceeds a hundred characters must not be accepeted, "
    			+ "Customer Name that exceeds a hundred characters must not be accepeted.\" }";
    	this.mvc.perform(post("/v1/customers").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isBadRequest());
    }

}