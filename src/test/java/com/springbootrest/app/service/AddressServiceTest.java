package com.springbootrest.app.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.springbootrest.app.NotFoundException;
import com.springbootrest.app.entity.Address;
import com.springbootrest.app.repository.AddressRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AddressService.class) 
public class AddressServiceTest {
	
	@Autowired
    private AddressService addressService;

	@MockBean
    private AddressRepository addressRepository;
	
	@Test
    public void createAnValidAddressShouldSuccess()
      throws Exception {
    	Address givenAddress = new Address();
    	givenAddress.setCity("CityName");
    	givenAddress.setState("SC");
    	
    	when(addressRepository.findAddressByCityAndStateIgnoreCase(any(), any())).thenReturn(Optional.ofNullable(null));
    	when(addressRepository.save(any())).thenReturn(new Address(1L, "CityName", "SC"));
    	
    	Address address = addressService.createAddress(givenAddress);
    	
    	assertThat(address.getCity()).isEqualTo("CityName");
    }
	
	@Test
    public void findByIdShouldSuccess()
      throws Exception {
    	
    	when(addressRepository.findById(5L)).thenReturn(Optional.of(new Address(5L, "CityName", "SC")));
    	
    	Address address = addressService.findById(5L);
    	
    	assertThat(address.getCity()).isEqualTo("CityName");
    	assertThat(address.getState()).isEqualTo("SC");
    }
	
	@Test
    public void findByIdWhenAddressDoesNotExistShouldThrowsException()
      throws Exception {
    	
    	when(addressRepository.findById(5L)).thenReturn(Optional.ofNullable(null));
    	
    	try {
    		addressService.findById(5L);
    		throw new Exception("Test failed");
		} catch (NotFoundException e) {
			assertThat(e.getMessage()).isEqualTo("id-5");
	    }
    }
    
	
}
