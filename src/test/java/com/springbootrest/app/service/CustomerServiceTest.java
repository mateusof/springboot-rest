package com.springbootrest.app.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.springbootrest.app.NotFoundException;
import com.springbootrest.app.entity.Address;
import com.springbootrest.app.entity.Customer;
import com.springbootrest.app.repository.AddressRepository;
import com.springbootrest.app.repository.CustomerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerService.class) 
public class CustomerServiceTest {
	
	@Autowired
    private CustomerService customerService;

	@MockBean
    private CustomerRepository customerRepository;
	
	@MockBean
    private AddressRepository addressRepository;
	
	@Test
    public void createAnValidCustomerShouldSuccess()
      throws Exception {
		Customer customer = createCustomer();
    	
		when(addressRepository.findById(56L)).thenReturn(Optional.of(new Address(56L, "City Name", "AA")));
		when(customerRepository.save(any())).thenReturn(customer);
		
		Customer customerRequest = new Customer();
		customerRequest.setName("Customer Name");
		customerRequest.setGender("F");
		customerRequest.setBirthDate(new Date(20296000));
		customerRequest.setAddressId(56L);
		
    	Customer customerResult = customerService.createCustomer(customerRequest);
    	
    	assertThat(customerResult.getAddress().getCity()).isEqualTo("City Name");
    	assertThat(customerResult.getAddress().getState()).isEqualTo("AA");
    	assertThat(customerResult.getName()).isEqualTo("Customer Name");
    	assertThat(customerResult.getGender()).isEqualTo("F");
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date expectedbirthDate = null;
		try {
			expectedbirthDate = sdf.parse("14/04/1991");
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	assertThat(customerResult.getBirthDate()).isEqualTo(expectedbirthDate);
    }
	
	@Test
    public void getCustomersMustFilterByCustomersName()
      throws Exception {
		Customer customer1 = new Customer();
		customer1.setName("Name");
		Customer customer2 = new Customer();
		customer2.setName("Name");
    	
		when(customerRepository.findCustomerByNameIgnoreCase("Name")).thenReturn(Arrays.asList(customer1, customer2));
		
    	List<Customer> customersResult = customerService.getCustomersByName("Name");
    	
    	assertThat(customersResult.size()).isEqualTo(2);
    	assertThat(customersResult.get(0).getName()).isEqualTo("Name");
    	assertThat(customersResult.get(1).getName()).isEqualTo("Name");
    }
	
	@Test
    public void getCustomerByIdShouldSuccess()
      throws Exception {
		Customer customer = createCustomer();
    	
		when(customerRepository.findById(67L)).thenReturn(Optional.of(customer));
		
    	Customer customerResult = customerService.retrieveCustomer(67L);
    	
    	assertThat(customerResult.getName()).isEqualTo(customer.getName());
    	assertThat(customerResult.getBirthDate()).isEqualTo(customer.getBirthDate());
    	assertThat(customerResult.getGender()).isEqualTo(customer.getGender());
    	assertThat(customerResult.getAddress()).isEqualTo(customer.getAddress());
    }
	
	@Test
    public void updatePartialNameShouldSuccess()
      throws Exception {
		Customer customer = createCustomer();
		String newName = "New Name";
    	
		when(customerRepository.findById(68L)).thenReturn(Optional.of(customer));
		
		customer.setName(newName);
		
		when(customerRepository.save(customer)).thenReturn(customer);
		
    	Customer customerResult = customerService.partialUpdateName(68L, newName);
    	
    	assertThat(customerResult.getName()).isEqualTo(newName);
    }
	
	@Test
    public void updatePartialNameWhenCustomerDoesNotExistShouldThrowsException()
      throws Exception {
		Customer customer = createCustomer();
		String newName = "New Name";
    	
		when(customerRepository.findById(68L)).thenReturn(Optional.of(customer));
		
		customer.setName(newName);
		
		when(customerRepository.save(customer)).thenReturn(customer);
		
		try {
			customerService.partialUpdateName(70L, newName);
			throw new Exception("Test failed");
		}catch (NotFoundException e) {
			assertThat(e.getMessage()).isEqualTo("id-70");
		}
    }
	
	@Test
    public void deleteWhenCustomerDoesNotExistShouldThrowsException()
      throws Exception {
		try {
			customerService.deleteCustomer(80L);
			throw new Exception("Test failed");
		}catch (NotFoundException e) {
			assertThat(e.getMessage()).isEqualTo("id-80");
		}
    }
	
	private Customer createCustomer() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = sdf.parse("14/04/1991");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Customer customer = new Customer();
		customer.setBirthDate(date);
		customer.setGender("F");
		customer.setName("Customer Name");
		Address address = new Address();
		address.setCity("City Name");
		address.setState("AA");
		customer.setAddress(address);
		return customer;
	}
	
    
	
}
