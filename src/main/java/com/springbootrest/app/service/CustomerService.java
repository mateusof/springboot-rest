package com.springbootrest.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springbootrest.app.NotFoundException;
import com.springbootrest.app.entity.Address;
import com.springbootrest.app.entity.Customer;
import com.springbootrest.app.repository.AddressRepository;
import com.springbootrest.app.repository.CustomerRepository;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private AddressRepository addressRepository;
	
	public Customer createCustomer(Customer customer) {
		Optional<Address> address = addressRepository.findById(customer.getAddressId());
		
		if(!address.isPresent()) {
			throw new NotFoundException("Address Not Found -" + customer.getAddressId());
		}
		
		customer.setAddress(address.get());
		Customer savedCustomer = customerRepository.save(customer);

		return savedCustomer;
	}
	
	public List<Customer> getCustomersByName(String name) {
		List<Customer> customer = customerRepository.findCustomerByNameIgnoreCase(name);
		return customer;
	}
	
	public Customer retrieveCustomer(long id) {
		Optional<Customer> customer = customerRepository.findById(id);

		if (!customer.isPresent())
			throw new NotFoundException("id-" + id);

		return customer.get();
	}
	
	public Customer partialUpdateName(long id, String name) {
		Optional<Customer> customer = customerRepository.findById(id);

		if (!customer.isPresent())
			throw new NotFoundException("id-" + id);
		
		Customer updateCustomer = customer.get();
		updateCustomer.setName(name);
		return customerRepository.save(updateCustomer);
	}
	
	public void deleteCustomer(long id) {
		Optional<Customer> customer = customerRepository.findById(id);

		if (!customer.isPresent())
			throw new NotFoundException("id-" + id);
		customerRepository.deleteById(id);
	}

}
