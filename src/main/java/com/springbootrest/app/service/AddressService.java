package com.springbootrest.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springbootrest.app.ConflictException;
import com.springbootrest.app.NotFoundException;
import com.springbootrest.app.entity.Address;
import com.springbootrest.app.repository.AddressRepository;

@Service
public class AddressService {
	
	@Autowired
	private AddressRepository addressRepository;
	
	public Address createAddress(Address address) {
		Optional<Address> existingAddress = 
				addressRepository.findAddressByCityAndStateIgnoreCase(address.getCity(), address.getState());
		
		if(existingAddress.isPresent())
			throw new ConflictException("address already exists");
		
		Address savedAddress = addressRepository.save(address);

		return savedAddress;
	}
	
	public List<Address> findAddressByCityOrState(String city, String state){
		return addressRepository.findAddressByCityOrStateIgnoreCase(city, state);
	}
	
	public Address findById(long id){
		Optional<Address> address = addressRepository.findById(id);

		if (!address.isPresent())
			throw new NotFoundException("id-" + id);

		return address.get();
	}
}
