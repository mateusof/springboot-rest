package com.springbootrest.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Address {
	@Id
	@GeneratedValue
	@ApiModelProperty(hidden= true)
	private Long id;
	@NotBlank
    @Size(max=30, message="City should maximum of 30 characters")
	private String city;
	@NotNull
    @Size(min=2, max=2, message="State should have 2 characters")
	private String state;
	
	public Address() {
		super();
	}
	
	public Address(Long id, 
			String city,
			String state) {
		super();
		this.id = id;
		this.city = city;
		this.state = state;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}	
}
