package com.springbootrest.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springbootrest.app.entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long>{
	
	Optional<Address> findAddressByCityAndStateIgnoreCase(String city, String state);
	
	List<Address> findAddressByCityOrStateIgnoreCase(String city, String state);
}
