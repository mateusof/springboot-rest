package com.springbootrest.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springbootrest.app.entity.Address;
import com.springbootrest.app.service.AddressService;

@RestController
@RequestMapping("v1/addresses")
public class AddressController {
	
	@Autowired
	private AddressService addressService;

	@PostMapping
	public Address createAddress(@Valid @RequestBody Address address) {
		return addressService.createAddress(address);
	}
	
	@GetMapping
	public List<Address> getAddresses(@RequestParam(required = false) String city, 
			@RequestParam(required = false) String state) {
		return addressService.findAddressByCityOrState(city, state);
	}
	
	@GetMapping("{id}")
	public Address retrieveAddress(@PathVariable long id) {
		return addressService.findById(id);
	}
}
