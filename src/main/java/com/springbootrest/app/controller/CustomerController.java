package com.springbootrest.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springbootrest.app.entity.Customer;
import com.springbootrest.app.service.CustomerService;

@RestController
@RequestMapping("v1/customers")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;

	@PostMapping
	public Customer createCustomer(@Valid @RequestBody Customer customer) {
		return customerService.createCustomer(customer);
	}
	
	@GetMapping
	public List<Customer> getCustomers(@RequestParam(required = false) String name) {
		return customerService.getCustomersByName(name);
	}
	
	@GetMapping("{id}")
	public Customer retrieveCustomer(@PathVariable long id) {
		return customerService.retrieveCustomer(id);
	}
	
	@PatchMapping("{id}")
	public Customer partialUpdateName(@PathVariable long id, @RequestBody String name) {
		return customerService.partialUpdateName(id, name);
	}
	
	@DeleteMapping("{id}")
	public void deleteCustomer(@PathVariable long id) {
		customerService.deleteCustomer(id);
	}
}