# Application to manage Customer and Address using Spring boot

## Installing

First, you need to clone the following repository `https://gitlab.com/mateusof/springboot-rest`.

## Running the Tests

The tests are located in src/test/java, run them using junit.

## Running the Project

In order to run the project you will need to run the Application class.

Access http://localhost:8080/swagger-ui.html for API documentation.